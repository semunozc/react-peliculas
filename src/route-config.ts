import CrearActors from "./actores/CrearActores";
import EditarActors from "./actores/EditarActores";
import IndiceActores from "./actores/IndiceActores";

import CrearCines from "./cines/CrearCine";
import EditarCines from "./cines/EditarCine";
import IndiceCines from "./cines/IndiceCines";

import CrearGeneros from "./generos/CrearGenero";
import EditarGeneros from "./generos/EditarGenero";
import IndiceGeneros from "./generos/IndiceGeneros";

import LandingPage from "./LandingPage";

import EditarPeliculas from "./peliculas/EditarPeliculas";
import FiltroPeliculas from "./peliculas/FiltroPeliculas";
import CrearPeliculas from "./peliculas/CrearPeliculas";
import RedireccionarALanding from "./utils/RedireccionarALanding";
import DetallePelicula from "./peliculas/DetallePelicula";

const Rutas = [

    { path :  '/generos/crear', componete: CrearGeneros },
    { path :  '/generos/editar/:id(\\d+)', componete: EditarGeneros },
    { path :  '/generos', componete: IndiceGeneros , exact: true },

    { path :  '/actores/editar/:id(\\d+)', componete: EditarActors },
    { path :  '/actores/crear', componete: CrearActors },
    { path :  '/actores', componete: IndiceActores , exact: true},

    { path :  '/pelicula/:id(\\d+)', componete: DetallePelicula },
    { path :  '/cines/editar/:id(\\d+)', componete: EditarCines },
    { path :  '/cines/crear', componete: CrearCines },
    { path :  '/cines', componete: IndiceCines , exact: true },
    
    { path :  '/peliculas/editar/:id(\\d+)', componete: EditarPeliculas },
    { path :  '/peliculas/crear', componete: CrearPeliculas },
    { path :  '/peliculas/filtrar', componete: FiltroPeliculas },
    
    {  path : '/', componete: LandingPage, exact: true },
    {  path : '*', componete: RedireccionarALanding, exact: true },
];
export default Rutas;