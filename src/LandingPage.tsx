import { useState, useEffect } from "react";
import { LandingPageDTO } from './peliculas/Peliculas.model'
import ListadoPeliculas from './peliculas/ListadoPeliculas'
import { urlPeliculas } from "./utils/endpoints";
import axios, { AxiosResponse } from "axios";
import AlertarContext from './utils/AlertaContext'

export default function LandingPage() {

    const [peliculas, setPeliculas] = useState<LandingPageDTO>({})

    useEffect(() => {
        cargarDatos();
    }, [])

    function cargarDatos() {
        axios.get(urlPeliculas)
            .then((respuesta: AxiosResponse<LandingPageDTO>) => {
                console.log(respuesta.data)
                setPeliculas(respuesta.data);
            })
    }

    return (
        <>
        <AlertarContext.Provider value={() => cargarDatos()}>
        <h3>En Cartelera</h3>
            <ListadoPeliculas peliculas={peliculas.enCines} />

            <h3>Próximos Estrenos</h3>
            <ListadoPeliculas peliculas={peliculas.proximosEstrenos} />
        </AlertarContext.Provider>

        </>
    )
}