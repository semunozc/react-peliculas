import { urlActores } from "../utils/endpoints";
import IndiceEntidad from "../utils/IndiceEntidad";
import { actorDTO } from "./actores.model";

export default function IndiceActores() {
  return (
    <>
      <IndiceEntidad<actorDTO>
        url={urlActores}
        urlCrear="actores/crear"
        titulo="Actores"
        nombreEntidad="Actor"
      >
        {(actores, botones) => (
          <>
            <thead>
              <tr>
                <th>Nombre</th>
                <th></th>
              </tr>
            </thead>
            <tbody>
              {actores?.map((actor) => (
                <tr key={actor.id}>
                  <td>{actor.nombre}</td>
                  <td>{botones(`actores/editar/${actor.id}`, actor.id)}</td>
                </tr>
              ))}
            </tbody>
          </>
        )}
      </IndiceEntidad>
    </>
  )}
