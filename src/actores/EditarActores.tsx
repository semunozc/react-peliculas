import React from "react";
import EditarEntidad from "../utils/EditarEntidad";
import { urlActores } from "../utils/endpoints";
import convertirActorAFormData from "../utils/FormDataUtils";
import { actorCreacionDTO, actorDTO } from "./actores.model";
import FormularioActores from "./FormularioActores";

export default function EditarActors() {

  const mapping = (actor: actorDTO) => {
    return {
        nombre: actor.nombre,
        fotoURL: actor.foto,
        biografia: actor.biografia,
        fechaNacimiento: new Date(actor.fechaNacimiento)
    }
}

  return (
    <>
        <EditarEntidad<actorCreacionDTO, actorDTO> url={urlActores} urlIndice="/actores" nombreEntidad="Actores" mapping={mapping} trasformarFormData={convertirActorAFormData}>
        {
          (entidad, editar) =>  <FormularioActores modelo={entidad} onSubmit={async (values) => await editar(values)} />
        }
        </EditarEntidad>
    </>
  );
}
