import axios, { AxiosResponse } from "axios";
import { useState, useEffect } from "react";
import { ReactElement } from "react-markdown";
import { useParams, useHistory } from "react-router-dom";
import Cargando from "./Cargando";
import MostrarErrores from "./MostrarErrores";

export default function EditarEntidad<T,R>(props: editarEntidadProps<T,R>){
    const {id}: any = useParams();
    const [entidad, setEntidad] = useState<T>()
    const [errores, setErrores] = useState<string[]>([])
    const history = useHistory()


    useEffect(() => {
        axios.get(`${props.url}/${id}`).then((response: AxiosResponse<R>) => {
            setEntidad(props.mapping(response.data))
        })
    // eslint-disable-next-line react-hooks/exhaustive-deps
    },[])
    const editar = async (entidadEditar: T) => {
        console.log(entidadEditar)
       try {
           if (props.trasformarFormData) {
            const formData = props.trasformarFormData(entidadEditar);
            await axios({
                method: 'put',
                url: `${props.url}/${id}`,
                data: formData,
                headers: {'Content-Type': 'multipart/form-data'}
            });
           }else{
            await axios.put(`${props.url}/${id}`, entidadEditar)
           }
         history.push(props.urlIndice)
       } catch (error) {
         setErrores(error.response.data)
       }
      }
      return (
        <>
            <h3>Editar {props.nombreEntidad}</h3>
            <MostrarErrores errores={errores} />
            {
                entidad ? props.children(entidad, editar) : <Cargando />
            }
        </>    
        )
}

interface editarEntidadProps<T,R> {
    url: string;
    urlIndice: string;
    nombreEntidad: string;
    children(entidad: T, editar : (entidad : T) => void) : ReactElement;
    mapping(entidad: R) : T;
    trasformarFormData? (modelo: T) : FormData
}

EditarEntidad.defaultProps = {
    mapping : (entidad : any) => entidad
}