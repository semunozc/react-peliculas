import React, { ReactElement } from "react";
import Cargando from "./Cargando";

export default function ListadoGenerico(props: listadoGenericoProps){
    if (!props.listado){
        if (props.cargandoUI){
            return  props.cargandoUI
        }
            return <Cargando/>
    }
    if (props.listado.length === 0) {
        if (props.ListadoVacioUI){
            return  props.ListadoVacioUI
        }
        return <>No hay elementos para mostrasr.</>
    }
    return props.children
}
interface listadoGenericoProps {
    listado: any;
    children: ReactElement;
    cargandoUI?: ReactElement;
    ListadoVacioUI?:ReactElement
}