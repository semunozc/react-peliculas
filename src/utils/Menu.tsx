import { NavLink } from "react-router-dom";

export default function Menu() {
  const claseActiva = "activa";
  return (
    <nav className="navbar navbar-expand-lg navbar-light gb-light">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/" activeClassName={claseActiva}>
          React Peliculas
        </NavLink>
        <div className="collapse navbar-collapse">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
            <li className="nav-item">
              <NavLink className="nav-link" activeClassName={claseActiva} to="/generos" > Generos </NavLink>
            </li>
            <li>
              <NavLink className="nav-link" activeClassName={claseActiva} to="/peliculas/crear" > Crear Peliculas </NavLink>
            </li>
            <li>
              <NavLink className="nav-link" activeClassName={claseActiva} to="/actores" > Actores </NavLink>
            </li>
            <li>
              <NavLink className="nav-link" activeClassName={claseActiva} to="/cines" > Cines </NavLink>
            </li>
            <li>
              <NavLink className="nav-link" activeClassName={claseActiva} to="/peliculas/filtrar" > Filtrar Peliculas </NavLink>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  );
}
