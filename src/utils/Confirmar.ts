import Swal from 'sweetalert2';

 const confirmar = (onConfirm:any, titulo: string = 'Seguro que desea eliminar el registro?', textoBotonConfirmacion: string = 'Eliminar') => {
    Swal.fire({
        title: titulo,
        confirmButtonText: textoBotonConfirmacion,
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33'
    }).then(result => {
        if (result.isConfirmed){
            onConfirm();
        }
    })
}

export default confirmar;