import { urlCines } from "../utils/endpoints";
import IndiceEntidad from "../utils/IndiceEntidad";
import { cineDTO } from "./cines.model";

export default function IndiceCines() {
  return (
    <>
      <IndiceEntidad<cineDTO> url={urlCines} urlCrear="cines/crear" titulo="Cines" nombreEntidad="Cine" >
        {(cines, botones) => (
          <>
            <thead>
              <tr>
                <th>Nombre</th>
                <th></th>
              </tr>
            </thead>
            <tbody>
              {cines?.map((g) => (
                <tr key={g.id}>
                  <td>{g.nombre}</td>
                  <td>{botones(`cines/editar/${g.id}`, g.id)}</td>
                </tr>
              ))}
            </tbody>
          </>
        )}
      </IndiceEntidad>
    </>
  )
}
