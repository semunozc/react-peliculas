import FormularioGeneros from "./FormularioGeneros";
import { urlGeneros } from "../utils/endpoints";
import { generoCreacionDTO, generoDTO } from "./Generos.model";
import EditarEntidad from "../utils/EditarEntidad";

export default function EditarGeneros() {

    return (
        <>
        <EditarEntidad<generoCreacionDTO, generoDTO> url={urlGeneros} urlIndice="/generos" nombreEntidad="Generos" >
            {
              (entidad, editar) =>  <FormularioGeneros modelo={entidad} onSubmit={ async valores => { await editar(valores) } } />
            }
        </EditarEntidad>
        </>
    )
  }
  
  