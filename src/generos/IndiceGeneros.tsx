import { urlGeneros } from "../utils/endpoints";
import IndiceEntidad from "../utils/IndiceEntidad";
import { generoDTO } from "./Generos.model";

export default function IndiceGeneros() {
  return (
    <>
      <IndiceEntidad<generoDTO> url={urlGeneros} urlCrear="generos/crear" titulo="Generos" nombreEntidad="Genero" >
        {(generos, botones) => (
          <>
            <thead>
              <tr>
                <th>Nombre</th>
                <th></th>
              </tr>
            </thead>
            <tbody>
              {generos?.map((g) => (
                <tr key={g.id}>
                  <td>{g.nombre}</td>
                  <td>{botones(`generos/editar/${g.id}`, g.id)}</td>
                </tr>
              ))}
            </tbody>
          </>
        )}
      </IndiceEntidad>
    </>
  );
}
