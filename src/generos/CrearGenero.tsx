import axios from "axios";
import React, { useState } from "react";
import { useHistory } from "react-router-dom";
import { urlGeneros } from "../utils/endpoints";
import MostrarErrores from "../utils/MostrarErrores";
import FormularioGeneros from "./FormularioGeneros";
import { generoCreacionDTO } from "./Generos.model";


export default function CrearGeneros() {
  const history = useHistory();
  const [errores, setErrores] = useState<string[]>([])
  const add = async (genero: generoCreacionDTO) => {
       try {
         await axios.post(urlGeneros, genero)
         history.push('/generos');
       } catch (error) {
         setErrores(error.response.data)
       }
    }

  return (
    <>
      <h3>Crear Genero</h3>
      <MostrarErrores  errores={errores} />
      <FormularioGeneros
        modelo={{ nombre: "" }}
        onSubmit={async (valores) => {
          await add(valores);
        }}
      />
    </>
  );
}
