import { Formik, Form, FormikHelpers } from "formik";
import React from "react";
import { Link } from "react-router-dom";
import Button from "../utils/Button";
import * as Yup from "yup";
import FormGroupText from "../utils/FormGrupText";
import { generoCreacionDTO } from "./Generos.model";

export default function FormularioGeneros(props: formularioGenerosProps ) {
    return (
        <Formik
        initialValues={props.modelo}
        onSubmit={props.onSubmit}
        validationSchema={Yup.object({
          name: Yup.string()
          .max(50,"La longitud maxima es de 50 caracteres")
            .required("Este campo es requerido.")
            .primeraLetraMayuscula(),
        })}
      > 
        {(formikProps) => (
          <Form>
            <FormGroupText
              campo="name"
              label="Nombre"
              placeholder="Nombre Genero"
            />
            <Button disabled={formikProps.isSubmitting} type="submit">Enviar</Button>
            <Link className="btn btn-secondary" to="/generos">
              Cancelar
            </Link>
          </Form>
        )}
      </Formik>
    )
}
interface formularioGenerosProps {
    modelo : generoCreacionDTO;
    onSubmit(valores : generoCreacionDTO, accion: FormikHelpers<generoCreacionDTO>): void
}