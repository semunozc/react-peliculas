import { cineDTO } from './../cines/cines.model.d';
import { generoDTO } from './../generos/Generos.model.d';
export interface PeliculaDTO{
    id: number;
    titulo: string;
    poster: string;
    enCines: boolean;
    trailer: string;
    resumen?: string;
    fechaLanzamiento: Date;
    cines: cineDTO[];
    generos: generoDTO[];
    actores: actorPeliculaDTO[];
}

export interface PeliculaCreacionDTO {
    titulo: string;
    enCines: boolean;
    trailer: string;
    fechaLanzamiento?: Date;
    poster?: File;
    resumen?: string;
    posterURL?: string;
    generosIds?: number[];
    cinesIds?: number[];
    actores?: actorPeliculaDTO[];
}

export interface LandingPageDTO {
    enCines?: pelicula[];
    proximosEstrenos?: pelicula[];
}

export interface PeliculasPostGetDTO {
    generos: generoDTO[];
    cines: cineDTO[];
}

export interface peliculasPutGetDTO {
    pelicula: peliculaDTO;
    generosSeleccionados: generoDTO[];
    generosNoSeleccionados: generoDTO[];
    cinesSeleccionados: cineDTO[];
    cinesNoSeleccionados: cineDTO[];
    actores: actorPeliculaDTO[];
}